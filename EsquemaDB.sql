
/*Estare usando DB First*/

create database ComprobantesNCF;


--creacion de las tablas.
create table ComprobanteFiscales(
Id INT PRIMARY KEY Identity,
RncCedula NVARCHAR(11) not null,
NCF NVARCHAR(13) not null,
Monto DECIMAL(18, 2),
Itbis18 DECIMAL(18, 2)
);


create table Contribuyente(
RncCedula NVARCHAR(11) PRIMARY KEY,
Nombre NVARCHAR(100) NOT NULL,
Tipo NVARCHAR(50) NOT NULL,
Estatus NVARCHAR(50) NOT NULL
);

--Insertando registros a las tablas.
INSERT INTO Contribuyente (RncCedula,Nombre,Tipo,Estatus)
VALUES ('98754321012','JUAN PEREZ','PERSONA FISICA','activo')
GO
INSERT INTO Contribuyente (RncCedula,Nombre,Tipo,Estatus)
VALUES ('123456789','FARMACIA TU SALUD','PERSONA JURIDICA','inactivo')


INSERT INTO ComprobanteFiscales(RncCedula,NCF,Monto,Itbis18)
VALUES('98754321012','E310000000001',200.00,36.00)
GO
INSERT INTO ComprobanteFiscales(RncCedula,NCF,Monto,Itbis18)
VALUES('98754321012','E310000000002',1000.00,180.00)

