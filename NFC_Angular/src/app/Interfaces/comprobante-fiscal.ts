export interface ComprobanteFiscal {
    rncCedula: string;
    ncf: string;
    monto: number;
    itbis18: number;
}
