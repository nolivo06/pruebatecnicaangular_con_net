import { ComprobanteFiscal } from "./comprobante-fiscal";

export interface ComprobanteFiscalResponse {
    comprobantes:ComprobanteFiscal[];
    sumaItbis:number;
}
