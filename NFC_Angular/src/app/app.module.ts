import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { AppRoutingModule } from './app.routes';

import { AppComponent } from './app.component';

import { ReactiveFormsModule } from '@angular/forms';

import {HttpClientModule} from '@angular/common/http';
import { ComprobanteFiscalComponent } from './comprobante-fiscal/comprobante-fiscal.component';
import { ContribuyenteComponent } from './contribuyente/contribuyente.component';

@NgModule({
  declarations: [
    AppComponent,
    ComprobanteFiscalComponent,
    ContribuyenteComponent
  ],
  imports: [
    BrowserModule,
    ReactiveFormsModule,
    HttpClientModule,
    FormsModule,
    RouterModule,
    AppRoutingModule
    
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
