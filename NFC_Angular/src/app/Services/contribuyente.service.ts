import { Injectable } from '@angular/core';
import{HttpClient} from '@angular/common/http';
import { environment } from 'src/environments/environment';
import {Observable} from 'rxjs';
import { Contribuyente } from '../Interfaces/contribuyente';

@Injectable({
  providedIn: 'root'
})
export class ContribuyenteService {


  private endpoint:string = environment.endPoint;
  private apiUrl:string = this.endpoint + "Contribuyente/";

  constructor(private http:HttpClient) { }

  getListContribuyent():Observable<Contribuyente[]>{
    return this.http.get<Contribuyente[]>(`${this.apiUrl}GetListContribuyent`);
  }

  addContribuyent(request:Contribuyente):Observable<Contribuyente>{
    return this.http.post<Contribuyente>(`${this.apiUrl}SaveContribuyente`,request);
  }

  deleteContribuyent(rnc:string):Observable<void>{
    return this.http.delete<void>(`${this.apiUrl}DeleteContribuyent/${rnc}`);
  }

  updateContribuyent(rnc:string, requestBody:any):Observable<any>{
    return this.http.put<any>(`${this.apiUrl}UpdateContribuyente/${rnc}`,requestBody);
  }
}
