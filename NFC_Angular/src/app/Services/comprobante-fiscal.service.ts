import { Injectable } from '@angular/core';
import{HttpClient} from '@angular/common/http';
import { environment } from 'src/environments/environment';
import {Observable} from 'rxjs';
import { ComprobanteFiscal } from '../Interfaces/comprobante-fiscal';
import { ComprobanteFiscalResponse } from '../Interfaces/comprobante-fiscal-response';

@Injectable({
  providedIn: 'root'
})
export class ComprobanteFiscalService {

  private endpoint:string = environment.endPoint;
  private apiUrl:string = this.endpoint + "NCF/";

  constructor(private http:HttpClient) { }

  getComprobantesPorRnc(rnc: string): Observable<ComprobanteFiscalResponse> {
    return this.http.get<ComprobanteFiscalResponse>(`${this.apiUrl}GetComprobantByRnc/${rnc}`);
  }

  addNCF(request:ComprobanteFiscal):Observable<ComprobanteFiscal>{
    return this.http.post<ComprobanteFiscal>(`${this.apiUrl}SaveComprobanteFiscales`,request);
  }
  
}
