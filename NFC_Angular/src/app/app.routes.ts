import { NgModule } from '@angular/core';
import { RouterModule,Routes } from '@angular/router';
import { ComprobanteFiscalComponent } from './comprobante-fiscal/comprobante-fiscal.component';
import { ContribuyenteComponent } from './contribuyente/contribuyente.component';



export const routes: Routes = [
    {path: 'contribuyente',component:ContribuyenteComponent},
    {path: 'comprobante-fiscal',component:ComprobanteFiscalComponent},
];

@NgModule({
    imports:[RouterModule.forRoot(routes)],
    exports:[RouterModule]
})
export class AppRoutingModule{}