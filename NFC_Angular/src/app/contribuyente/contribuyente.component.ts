
import { Component, OnInit } from '@angular/core';
import { Contribuyente } from '../Interfaces/contribuyente';
import { ContribuyenteService } from '../Services/contribuyente.service';
import {FormBuilder,FormGroup,Validators} from '@angular/forms';

@Component({
  selector: 'app-contribuyente',
  templateUrl: './contribuyente.component.html',
  styleUrls: ['./contribuyente.component.css']
})
export class ContribuyenteComponent implements OnInit {
 //Contribuyentes
 listaContribuyente: Contribuyente[] = [];
 formularioContribuyente: FormGroup;
 nombre:string ='';
 tipo:string='';
 estatus:string='';


  constructor(private _contribuyenteServicio:ContribuyenteService,
    private _fb:FormBuilder) { 
      this.formularioContribuyente = this._fb.group({
        rncCedula:['',Validators.required],
        nombre:['',Validators.required],
        tipo:['',Validators.required],
        estatus:['',Validators.required]
      });
    }


    obtenerContribuyente(){
      this._contribuyenteServicio.getListContribuyent().subscribe({
        next:(data)=>{
          this.listaContribuyente = data;
          console.log(this.listaContribuyente);
        },error:(e)=>{
          console.log(e);
        }
      });
    }

  ngOnInit(): void {
    this.obtenerContribuyente();
  }

  agregarContribuyente(){
    const request:Contribuyente={
      rncCedula: this.formularioContribuyente.value.rncCedula,
      nombre: this.formularioContribuyente.value.nombre,
      tipo: this.formularioContribuyente.value.tipo,
      estatus: this.formularioContribuyente.value.estatus
    }
    this._contribuyenteServicio.addContribuyent(request).subscribe({
      next:(data)=>{
        this.listaContribuyente.push(data);
        this.formularioContribuyente.patchValue({
          rncCedula:"",
          nomnre:"",
          tipo:"",
          estatus:""
        });
      },error:(e)=>{
        console.log(e);
      }
    });

  }


  actualizarContribuyente(rnc: string):void{
    const requestBody ={
      nombre: this.formularioContribuyente.get('nombre')?.value,
      tipo: this.formularioContribuyente.get('tipo')?.value,
      estatus: this.formularioContribuyente.get('estatus')?.value
    };

    this._contribuyenteServicio.updateContribuyent(rnc, requestBody)
      .subscribe(
        (response)=>{
          console.log(response);
        },
        (error) =>{
          console.log(error);
        }
      )
  }

  seleccionarContribuyente(contribuyente:Contribuyente) :void{
    this.formularioContribuyente.patchValue(contribuyente);
  }


  eliminarContribuyente(rnc:string){
    this._contribuyenteServicio.deleteContribuyent(rnc).subscribe({
      next:(data)=>{
        const nuevaLista = this.listaContribuyente.filter(item => item.rncCedula != rnc);
        this.listaContribuyente = nuevaLista;
      },error:(e)=>{
        console.log(e);
      }
    });
  }

}
