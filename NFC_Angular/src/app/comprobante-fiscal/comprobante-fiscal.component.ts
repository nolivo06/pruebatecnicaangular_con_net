import { Component, OnInit } from '@angular/core';
import {FormBuilder,FormGroup,Validators} from '@angular/forms';

import { ComprobanteFiscalService } from '../Services/comprobante-fiscal.service';
import { ComprobanteFiscal } from '../Interfaces/comprobante-fiscal';
import { ComprobanteFiscalResponse } from '../Interfaces/comprobante-fiscal-response';


@Component({
  selector: 'app-comprobante-fiscal',
  templateUrl: './comprobante-fiscal.component.html',
  styleUrls: ['./comprobante-fiscal.component.css']
})
export class ComprobanteFiscalComponent implements OnInit {

    //NCF
    comprobantes: ComprobanteFiscal[] = [];
    sumaitbis: number = 0;
    rnc: string = '';
    error:string |null = null;
    formularioNCF: FormGroup;

  constructor(private _comprobanteFiscalService:ComprobanteFiscalService,
    private _fb:FormBuilder) { 
      this.formularioNCF = this._fb.group({
        rncCedula:['',Validators.required],
        ncf:['',Validators.required],
        monto:['',Validators.required],
        itbis18:['',Validators.required]
      });
  }

  ngOnInit(): void {
  }

  
  getComprobante(rnc: string): void {
    this.comprobantes =[];
    this.sumaitbis =0;
    this.error=null;
    this._comprobanteFiscalService.getComprobantesPorRnc(rnc)
      .subscribe(
        ({ comprobantes, sumaItbis }: ComprobanteFiscalResponse) => {
          this.comprobantes = comprobantes;
          this.sumaitbis = sumaItbis;
          this.error=null;
        },
        (error) => {
          console.error('Error al obtener los comprobantes:', error);
          this.error = `No es encontraron comprobantes relacionados con el RNC: ${rnc}`;
        }
      );
  }


  agregarContribuyente():void{
    if (this.formularioNCF.invalid) {
      // Si el formulario es inválido, no hagas nada
      return;
    }
    const request:ComprobanteFiscal={
      rncCedula: this.formularioNCF.value.rncCedula,
      ncf: this.formularioNCF.value.ncf,
      monto: this.formularioNCF.value.monto,
      itbis18: this.formularioNCF.value.itbis18
    };
    this._comprobanteFiscalService.addNCF(request).subscribe({
      next:(data)=>{
        console.log('Registro de Comprobante Fiscal agregado:', data);
      },error:(e)=>{
        console.log(e);
      }
    });

  }

}

