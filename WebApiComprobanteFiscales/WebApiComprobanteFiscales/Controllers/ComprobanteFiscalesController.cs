﻿using Microsoft.AspNetCore.Mvc;
using WebApiCore.DTO;
using WebApiCore.Interfaces;

namespace WebApiComprobanteFiscales.Controllers
{
    [ApiController]
    [Route("api/NCF")]
    public class ComprobanteFiscalesController: ControllerBase
    {
        private readonly IServicesNCF services;
        private readonly ILogger<ContribuyenteController> logger;

        public ComprobanteFiscalesController(IServicesNCF services,
            ILogger<ContribuyenteController> logger)
        {
            this.services = services;
            this.logger = logger;
        }



        [HttpGet("GetComprobantByRnc/{rnc}")]
        public async Task<ActionResult<ComprobantesConSumaItbis>> GetComprobanteByRnc(string rnc)
        {
            try
            {
                var resultado = await services.GetComprobanteFiscalesByRnc(rnc);

                if (!resultado.Comprobantes.Any())
                {
                    return NotFound($"No se encontraron comprobantes con el RNC: {rnc}");
                }

                return Ok(resultado);
            }
            catch (Exception ex)
            {
                logger.LogError($"Error al obtener los comprobantes por cedula:{ex.Message}");


                return StatusCode(500, $"Error al obtener los comprobantes por cedula");

            }
        }


        [HttpPost("SaveComprobanteFiscales")]
        public async Task<IActionResult> SaveComprobanteFiscales(ComprobanteFiscalAddDTO comprobanteFiscalAdd)
        {
            try
            {
                var result = await services.SaveNCF(comprobanteFiscalAdd);

                if (result)
                    return Ok("El registro ha sido guardado con exito");
                else
                {
                    return StatusCode(500, "Error al guardar el registro");
                }
            }
            catch (Exception ex)
            {
                logger.LogError($"Error al intentar guardar el registro: {ex.Message}");
                return StatusCode(500, "Error al intentar guardar el registro");
            }
        }

        [HttpPut("UpdateComprobanteFiscales/{id:int}")]
        public async Task<IActionResult> UpdateComprobanteFiscales(int id, ComprobanteFiscalAddDTO comprobanteFiscalAdd)
        {
            try
            {
                var result = await services.UpdateNCF(id, comprobanteFiscalAdd);

                if (result) return Ok("El registro ha sido actualizado exitosamente");
                else
                {
                    return NotFound("El registro no se encuentra");
                }
            }
            catch (Exception ex)
            {
                logger.LogError($"Error al intentar actualizar el registro: {ex.Message}");
                return StatusCode(500, "Error al intentar actualizar el registro");
            }
        }


        [HttpDelete("DeleteComprobanteFiscales/{id:int}")]
        public async Task<IActionResult> DeleteContribuyente(int id)
        {
            try
            {
                var result = await services.DeleteNCF(id);
                if (result) return Ok("El registro ha sido eliminado correctamente");
                else
                {
                    return NotFound("El registro no ha sido encontrado");
                }
            }
            catch (Exception ex)
            {
                logger.LogError($"Error al intentar eliminar el registro {ex.Message}");
                return StatusCode(500, "Error al eliminar el registro");
                throw;
            }
        }
    }
}
