﻿using Microsoft.AspNetCore.Mvc;
using WebApiCore.DTO;
using WebApiCore.Interfaces;
using WebApiCore.Models;

namespace WebApiComprobanteFiscales.Controllers
{
    [ApiController]
    [Route("api/Contribuyente")]
    public class ContribuyenteController:ControllerBase
    {
        private readonly IServicesNCF services;
        private readonly ILogger<ContribuyenteController> logger;

        public ContribuyenteController(IServicesNCF services, 
            ILogger<ContribuyenteController> logger)
        {
            this.services = services;
            this.logger = logger;
        }

        [HttpGet]
        [Route("GetListContribuyent")]
        public async Task<ActionResult<IEnumerable<Contribuyente>>> GetListContribuyente()
        {
            try
            {
                var contribuyentes = await services.GetAllContribuyente();
                return Ok(contribuyentes);
            }
            catch (Exception ex)
            {
                logger.LogError($"Error al obtener el listado de los contribuyente:{ex.Message}");


                return StatusCode(500, $"Error al obtener el listado de los contribuyente");
            }
        }


        [HttpPost("SaveContribuyente")]
        public async Task<IActionResult> SaveContribuyente(ContribuyenteAddDTO contribuyenteAdd)
        {
            try
            {
                var result = await services.SaveContribuyente(contribuyenteAdd);

                if (result)
                    return Ok("Contribuyente guardado con exito");
                else
                {
                    return StatusCode(500, "Error al guardar el contribuyente");
                }
            }
            catch (Exception ex)
            {
                logger.LogError($"Error al intentar guardar el contribuyente: {ex.Message}");
                return StatusCode(500, "Error al intentar guardar el contribuyente");
            }
        }

        [HttpPut("UpdateContribuyente/{rnc}")]
        public async Task<IActionResult> UpdateContribuyente(string rnc, ContribuyenteUpdateDTO contribuyenteUpdate)
        {
            try
            {
                var result = await services.UpdateContribuyente(rnc, contribuyenteUpdate);

                if (result) return Ok("El contribuyente actualizado exitosamente");
                else
                {
                    return NotFound("El contribuyente no se encuentra");
                }
            }
            catch (Exception ex)
            {
                logger.LogError($"Error al intentar actualizar el contribuyente: {ex.Message}");
                return StatusCode(500, "Error al intentar actualizar el contribuyente");
            }
        }


        [HttpDelete("DeleteContribuyent/{rnc}")]
        public async Task<IActionResult> DeleteContribuyente(string rnc)
        {
            try
            {
                var result = await services.DeleteContribuyente(rnc);
                if (result) return Ok("El contribuyente eliminado correctamente");
                else
                {
                    return NotFound("El contribuyente no encontrado");
                }
            }
            catch (Exception ex)
            {
                logger.LogError($"Error al intentar eliminar el contribuyente {ex.Message}");
                return StatusCode(500,"Error al eliminar el contribuyente");
                throw;
            }
        }

    }
}
