﻿using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebApiCore.Interfaces;
using WebApiInfraestructure.Common.Services;

namespace WebApiInfraestructure.Common.ServicesCollection
{
    public static class ServiceCollectionExtension
    {

        public static void AddCommonLayerApi(this IServiceCollection services)
        {
            services.AddTransient<IServicesNCF, ServicesNCF>();
        }
    }
}
