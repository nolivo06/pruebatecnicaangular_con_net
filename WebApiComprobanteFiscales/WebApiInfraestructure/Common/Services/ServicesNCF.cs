﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Microsoft.Identity.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebApiCore.DTO;
using WebApiCore.Interfaces;
using WebApiCore.Models;
using WebApiInfraestructure.Persistence.Data;

namespace WebApiInfraestructure.Common.Services
{
    public class ServicesNCF : IServicesNCF
    {
        private readonly ApplicationDbContext context;
        private readonly IMapper mapper;
        private readonly ILogger<ServicesNCF> logger;

        public ServicesNCF(ApplicationDbContext context,
            IMapper mapper,
            ILogger<ServicesNCF> logger)
        {
            this.context = context;
            this.mapper = mapper;
            this.logger = logger;
        }


        #region Contribuyente
        public async Task<bool> DeleteContribuyente(string rnc)
        {
            try
            {
                var contribuyente = await context.Contribuyentes.FindAsync(rnc);

                if (contribuyente == null)
                {
                    return false;
                }

                context.Contribuyentes.Remove(contribuyente);
                await context.SaveChangesAsync();
                return true;
            }
            catch (Exception ex)
            {
                logger.LogError($"Error al guardar el contribuyente: {ex.Message}");
                return false;
            }
        }

        public async Task<IEnumerable<Contribuyente>> GetAllContribuyente()
        {
            try
            {
                var contribuyentesFromDB = await context.Contribuyentes.ToListAsync();

                var contribuyentes = mapper.Map<IEnumerable<Contribuyente>>(contribuyentesFromDB);

                return contribuyentes;
            }
            catch (Exception ex)
            {
                logger.LogError($"Error al guardar el contribuyente: {ex.Message}");
                return Enumerable.Empty<Contribuyente>();
            }
        }

        public async Task<bool> SaveContribuyente(ContribuyenteAddDTO contribuyenteAdd)
        {
            try
            {
                var contribuyente = mapper.Map<Contribuyente>(contribuyenteAdd);

                context.Contribuyentes.Add(contribuyente);

                await context.SaveChangesAsync();

                return true;
            }
            catch (Exception ex)
            {
                logger.LogError($"Error al guardar el contribuyente: {ex.Message}");
                return false;
            }
        }


        public async Task<bool> UpdateContribuyente(string rnc, ContribuyenteUpdateDTO contribuyenteUpdate)
        {
            try
            {
                var contribuyente = await context.Contribuyentes.FindAsync(rnc);

                if (contribuyente == null)
                {
                    return false;
                }

                contribuyente.Nombre = contribuyenteUpdate.Nombre;
                contribuyente.Tipo = contribuyenteUpdate.Tipo;
                contribuyente.Estatus = contribuyenteUpdate.Estatus;

                await context.SaveChangesAsync();

                return true;

            }
            catch (Exception ex)
            {

                logger.LogError($"Error al actualizar el contribuyente: {ex.Message}");
                return false;
            }

        }
        #endregion


        #region ComprobanteFiscales
        public async Task<bool> DeleteNCF(int id)
        {
            try
            {
                var comprobanteFiscales = await context.ComprobanteFiscales.FindAsync(id);

                if (comprobanteFiscales == null)
                {
                    return false;
                }

                context.ComprobanteFiscales.Remove(comprobanteFiscales);
                await context.SaveChangesAsync();
                return true;
            }
            catch (Exception ex)
            {
                logger.LogError($"Error al guardar el contribuyente: {ex.Message}");
                return false;
            }
        }

        public async Task<ComprobantesConSumaItbis> GetComprobanteFiscalesByRnc(string rnc)
        {
            var comprobantes = await context.ComprobanteFiscales.Where(x => x.RncCedula == rnc).ToListAsync();

            var comprobantesDTO = mapper.Map<List<ComprobanteFiscalesDTO>>(comprobantes);


            decimal sumaItbis = comprobantes.Sum(x => x.Itbis18);

            var result = new ComprobantesConSumaItbis
            {
                Comprobantes = comprobantesDTO,
                SumaItbis = sumaItbis
            };

            return result;
        }


        public async Task<bool> SaveNCF(ComprobanteFiscalAddDTO comprobanteFiscalAdd)
        {
            try
            {
                var comprobanteFiscal = mapper.Map<ComprobanteFiscales>(comprobanteFiscalAdd);

                context.ComprobanteFiscales.Add(comprobanteFiscal);
                await context.SaveChangesAsync();
                return true;
            }
            catch (Exception ex)
            {
                logger.LogError($"Error al guardar el registro: {ex.Message}");
                return false;
            }
        }



        public async Task<bool> UpdateNCF(int id, ComprobanteFiscalAddDTO comprobanteFiscalAdd)
        {
            try
            {
                var comprobanteFiscal = await context.ComprobanteFiscales.FindAsync(id);
                if (comprobanteFiscal == null)
                    return false;
                comprobanteFiscal.RncCedula = comprobanteFiscalAdd.RncCedula;
                comprobanteFiscal.NCF = comprobanteFiscalAdd.NCF;
                comprobanteFiscal.Monto = comprobanteFiscalAdd.Monto;
                comprobanteFiscal.Itbis18 = comprobanteFiscalAdd.Itbis18;

                await context.SaveChangesAsync();

                return true;
            }
            catch (Exception ex)
            {
                logger.LogError($"Error al actualizar el registro: {ex.Message}");
                return false;
            }
        }
        #endregion


    }
}
