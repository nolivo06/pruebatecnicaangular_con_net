﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebApiCore.Models;

namespace WebApiInfraestructure.Persistence.Data
{
    public class ApplicationDbContext:DbContext
    {    

        public ApplicationDbContext(DbContextOptions options) :base(options)
        {  
        }


        #region DbSet
        public DbSet<Contribuyente> Contribuyentes { get; set; }

        public DbSet<ComprobanteFiscales> ComprobanteFiscales { get; set; }

        #endregion

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<Contribuyente>(entity =>
            {
                entity.HasKey(e => e.RncCedula);
            });




            modelBuilder.ApplyConfigurationsFromAssembly(typeof(ApplicationDbContext).Assembly);
        }

    }
}
