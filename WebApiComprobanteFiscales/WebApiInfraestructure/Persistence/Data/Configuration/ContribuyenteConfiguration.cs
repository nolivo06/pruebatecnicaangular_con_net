﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebApiCore.Models;

namespace WebApiInfraestructure.Persistence.Data.Configuration
{
    public class ContribuyenteConfiguration: IEntityTypeConfiguration<Contribuyente>
    {
      
        public void Configure(EntityTypeBuilder<Contribuyente> builder)
        {
            builder.ToTable("Contribuyente");


            builder.Property(x => x.RncCedula)
                .HasMaxLength(11)
                .IsUnicode(false)
                .IsRequired();

            builder.Property(x => x.Nombre)
                .HasMaxLength(100)
                .IsUnicode(false)
                .IsRequired();
                
            builder.Property(x => x.Tipo)
                .HasMaxLength(50)
                .IsUnicode(false)
                .IsRequired();

            builder.Property(x => x.Estatus)
                .HasMaxLength(50)
                .IsUnicode(false)
                .HasDefaultValueSql("activo")
                .IsRequired();
        }
    }
}
