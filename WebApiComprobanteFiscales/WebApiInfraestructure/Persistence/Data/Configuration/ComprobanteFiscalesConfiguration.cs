﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebApiCore.Models;

namespace WebApiInfraestructure.Persistence.Data.Configuration
{
    public class ComprobanteFiscalesConfiguration:IEntityTypeConfiguration<ComprobanteFiscales>
    {
        public void Configure(EntityTypeBuilder<ComprobanteFiscales> builder)
        {
            builder.ToTable("ComprobanteFiscales");

            builder.Property(x => x.RncCedula)
                .HasMaxLength(11)
                .IsUnicode(false)
                .IsRequired();

            builder.Property(x => x.NCF)
                .HasMaxLength(13)
                .IsUnicode(false)
                .IsRequired();

            builder.Property(x => x.Monto)
                .IsUnicode(false);

            builder.Property(x => x.Itbis18)
                .IsUnicode(false);
        }
    }
}
