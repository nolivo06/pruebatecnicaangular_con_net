﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebApiCore.DTO;
using WebApiCore.Models;

namespace WebApiInfraestructure.Application.Mappings
{
    public class AutoMapperProfile:Profile
    {
        public AutoMapperProfile()
        {
            CreateMap<ComprobanteFiscales, ComprobanteFiscalesDTO>().ReverseMap();
            CreateMap<ContribuyenteAddDTO, Contribuyente>();
            CreateMap<ContribuyenteUpdateDTO, Contribuyente>();

            CreateMap<ComprobanteFiscalAddDTO, ComprobanteFiscales>();
        }
    }
}
