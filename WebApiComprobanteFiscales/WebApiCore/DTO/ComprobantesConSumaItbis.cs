﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebApiCore.Models;

namespace WebApiCore.DTO
{
    public class ComprobantesConSumaItbis
    {
        public IEnumerable<ComprobanteFiscalesDTO> Comprobantes { get; set; }
        public decimal SumaItbis { get; set; }
    }
}
