﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebApiCore.DTO;
using WebApiCore.Models;

namespace WebApiCore.Interfaces
{
    public interface IServicesNCF
    {
        #region Contribuyente
        Task<IEnumerable<Contribuyente>> GetAllContribuyente();
       
        Task<bool> SaveContribuyente(ContribuyenteAddDTO contribuyenteAdd);
        Task<bool> UpdateContribuyente(string rnc, ContribuyenteUpdateDTO contribuyenteUpdate);
        Task<bool> DeleteContribuyente(string rnc);
        #endregion

        #region NCF
        Task<ComprobantesConSumaItbis> GetComprobanteFiscalesByRnc(string rnc);
        Task<bool> SaveNCF(ComprobanteFiscalAddDTO comprobanteFiscalAdd);
        Task<bool> UpdateNCF(int id, ComprobanteFiscalAddDTO comprobanteFiscalAdd);
        Task<bool> DeleteNCF(int id);

        #endregion
    }
}
